.. _hackathons:

Hackathons
==========

:Owner: Samuel Warfield

We have four main types of events: weekly meetings, hackathons/competitions,
onboarding sessions, service events, and social events.

By far hackathons are by far the most logistically complex event that ACM
organizes. Failure to recognize this fact will result in **severe consquences**

Check List
----------

- [] @Officers Decide on a hackathon to visit (2 Months Out)
- [] @Officers Inform club of Hackathon (2 Months out)
- [] @Secretary Put out a Google form with the following (1.5 Months Out)
    - [] Full Legal Name (As it appears on lisence)
    - [] CWID
    - [] Gender
    - [] Date of Birth
- [] @Treasurer Assemble funding sources
- [] @Treasurer Order flights / buses (3+ Weeks Out)
- [] @President Send final reminder email with packing info as per Tips & Tricks
     (Three days out with final reminder 24 hrs out)
- [] Proceed to hackathon, you may pass GO

Tips & Tricks
-------------

These are the the secrets to having a good hackathon. Please email these to all
attending members. This can make or break the experience.

1. Hammocks, if you want to sleep at all bring one
#. Laptops, do not even think about going without one

.. TODO: Link to usefully programing libs

Flights
-------

**Avoid the CSM Travel Office**, they are very slow and will almost certainly be
more expensive than what you can find! The key to getting good flights is
booking through a specified airlines group booking service. For example I have 
Spirit Alines' group booking linke down below.

Resources
^^^^^^^^^
- `Google Flights <flights.google.com>`_
- `Spirit Groups <https://customersupport.spirit.com/hc/en-us/articles/360002248271-Group-Travel>`_

Leasons learned
---------------

The hard earned lessons we've found over the years. **Ignore at your own risk**

Flights
^^^^^^^

Order them early **(More than three weeks out)**. If you fail to do this you
fail to attend the hackathon. (Do not pass GO, procede to Gulag)