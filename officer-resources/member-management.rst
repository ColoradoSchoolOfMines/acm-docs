Member Management
=================

There's not any real procedure for becoming a member of ACM. Basically, if
someone comes to more than a few of our events, we consider them to be a member.
However, there are quite a few people who try and join using Engage.

Engage
------

Whoever has administrative permission on Engage should set it up to be notified
whenever someone asks to be added as a member to the site. When an email comes
in from Engage, follow this procedure:

1. Look up their username on DirSearch.
2. Add them to the mailing list using the Mailing List page on the ACM website
   (https://acm.mines.edu/mailinglist).
3. Accept their request to join the organization on Engage.
