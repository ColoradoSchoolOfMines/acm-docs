Meeting Food
============

:Owner: Sam Warfield

This is one of the most important responsibilities in Mines ACM. If there is no
food, people don't come. Thus, it is extremely important to make sure that some
food shows up at every meeting.

There are two ways to acquire the money for pizza, going through CS Department
funds or going through SAIL Allocation. Either way you **MUST CALL** Dominios.
All purchases with school funds are tax exempt, and there is no way to make tax
exempt orders online. You **MUST KEEP AN ITEMIZED RECEIPT FOR ALL PURCAHSES**.

CS@Mines funds
--------------
This is the easy form of getting pizza, contact `Shannon Roebuck <sroebuck@mines.edu>`_
and get her departmental credit card. When ordering specify that it is tax
exempt, and read the number printed on the upper right hand side of the card.

SAIL Funds
----------

Oh boy may the lord have mercy on your soul, who knows if SAIL will be the name
of this organization in a year.

How Much Pizza
--------------

- Normal Meetings
   - TBD
- Google Event
   - This event is the largest that ACM hosts, it will fill BBW280! So we need
     a lot of pizza. In 2017 we ordered 33 pizzas, 2018: 35 and 10-12 bottles of
     soda.

Documentation
-------------

All receipts are to be recorded to `this Google Spreadsheet
<https://drive.google.com/drive/folders/0B9Olhz9tDXhcTkxhZTF3QjJrYzQ?usp=sharing>`_.
Make sure this stays private, only accessible by the officers.
