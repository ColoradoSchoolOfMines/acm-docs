.. _events:

Events
======

:Owner: Sumner Evans

We have four main types of events: weekly meetings, hackathons/competitions,
onboarding sessions, service events, and social events.

All Events
----------

These events have a lot in common as far as logistics go.

- **Room:** (Jackson) every event needs a room. Reserve through TODO.
- **Email Notifications:** (Sumner) every meeting needs an email notification.
  This should go out the day before the event.
- **CS Weekly Notification:** (Sumner) every meeting needs to be on the CS
  Weekly. Email Kelly Knechtel <knechtel@mines.edu> to get events on the weekly.
- **Flyers:** (Sumner) every meeting needs a flyer.
- **Daily Blast** (Robby) see `Daily Blast`_

Weekly Meetings
---------------

- **Designated "welcome" officer:** (Sumner)

TODO: describe more info specific to these:

- Weekly Meetings

  - Tech Talks

- Hackathons/competitions
- Onboarding sessions

Social Events
-------------

:Owner: Sumner Evans

The goal of social events is to provide a catalyst for social integration into
our club.

TODO: flesh out

.. _service-events:

Service Events
--------------

:Owner: Sumner Evans

- Sheldon Math and Science Night
- ICPC
- Open Source Workshop
- CCIT Cyber Security Awareness

TODO: flesh out

Daily Blast
-----------

For every event, you need to submit the it to the Daily Blast. This is done at
https://webapps.mines.edu/DailyBlast/Home/BuildDigestItem. I used the following
settings:

- Department/Organization: Mines ACM
- Category: Campus Events / Meetings
- Audience: Faculty / Staff and Students
- Division: Student Life - Student Activities / Organizations
- Title:

  - For tech talks: **<company> - Tech Talk Tuesday - <talk_title> - FREE
    FOOD!**
  - For other events: **<event_name> - FREE FOOD!** (unless there is no free
    food, then don't add that)

- Brief description:

  - For tech talks: **Join Mines ACM as we host <name> from <company> for a
    presentation about <topic (can be longer description than title)>.**
  - For other events: *whatever seems logical*

- URL: https://acm.mines.edu/schedule
- Check the box to disable the long description, and instead go straight to the
  URL.

Make sure that the event is included on the Daily Blast on Friday and Monday on
the week of the event.
