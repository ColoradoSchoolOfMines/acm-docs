.. image:: https://gitlab.com/ColoradoSchoolOfMines/acm-brand/raw/master/logo/triangle/full_2048.png
   :width: 300px
   :target: https://acm.mines.edu
   :class: acm-logo

Welcome to the Mines ACM Documentation!
=======================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   officer-resources/index.rst
